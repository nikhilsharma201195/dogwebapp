import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegisterComponent } from "./register/register/register.component";
import { LoginComponent } from "./login/login/login.component";
import { AddDogComponent } from "./add-dog/add-dog/add-dog.component";
import { ListDogComponent } from "./list-dog/list-dog.component";
import { AuthenticationService } from "./service/auth.service";
import {
  HttpClientModule,
  HttpClient,
  HTTP_INTERCEPTORS
} from "@angular/common/http";
import { PetfinderService } from "./service/petfinder.service";
import { LoaderComponent } from "./shared/loader/loader.component";
import { ToastComponent } from "./shared/toast/toast.component";
import { ToastrModule } from "ngx-toastr";
import { AlertService } from "./service/alert.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ConsoleLoggerService } from "./service/console_logger.service";
import { LoggerService } from "./service/logger.service";
import { NotFoundComponent } from "./not-found/not-found.component";
import { INTERCEPTOR_PROVIDERS } from "./Interceptor/all_Intercepetor_container";
import { LoaderService } from './service/loader.service';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    AddDogComponent,
    ListDogComponent,
    LoaderComponent,
    ToastComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: "toast-top-right",
      timeOut: 1800,
      preventDuplicates: true
    }),
    BrowserAnimationsModule
  ],
  providers: [
    AuthenticationService,
    PetfinderService,
    AlertService,
    LoaderService,
    INTERCEPTOR_PROVIDERS,
    { provide: LoggerService, useClass: ConsoleLoggerService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
