import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { IDog } from "src/app/model/animal.model";
import { PetfinderService } from "src/app/service/petfinder.service";
import { AlertService } from "src/app/service/alert.service";
import { LoggerService } from "src/app/service/logger.service";
import { IAnimal } from "src/app/model/animal.model";

@Component({
  selector: "app-add-dog",
  templateUrl: "./add-dog.component.html",
  styleUrls: ["./add-dog.component.scss"]
})
export class AddDogComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    public petFinderService: PetfinderService,
    public alertservice: AlertService,
    public logger: LoggerService
  ) {
    this.logger.info("AddDogComponent: logger.info");
  }

  dogs: IDog[];
  animals: IAnimal[];
  addForm: FormGroup;
  breeds: any[];
  selectedBreed: string;
  error: string;
  alertmessage: string = `Dog breeds category list loaded Succesfully`;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      email: ["", Validators.required],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required]
    });
    this.getDogBreeds();
  }

  dogbreedSelector() {
    // this.petFinderService.isLoading = true;
    this.petFinderService.getDogsByBreed(this.selectedBreed).subscribe(
      (data: any) => {
        this.alertservice.showSuccess(
          `Data of ${this.selectedBreed} loaded Succesfully`
        );
        // this.petFinderService.isLoading = false;
        this.animals = data.animals;
      },
      error => {
        this.error = error;
        // this.petFinderService.isLoading = false;
      }
    );
  }

  getDogBreeds() {
    // this.petFinderService.isLoading = true;
    this.petFinderService.getBreeds().subscribe(
      (data: any) => {
        // this.petFinderService.isLoading = false;
        this.breeds = data.breeds;
        this.alertservice.showSuccess(this.alertmessage);
      },
      error => {
        this.error = error;
        // this.petFinderService.isLoading = false;
      }
    );
  }

  trackByFn(index, item) {
    return this.logger.info(item, index), index; // or item.id
  }
}
