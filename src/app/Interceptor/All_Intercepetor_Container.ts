import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { MyHttpInterceptor } from "./httpconfig.interceptor";
import { LoaderInterceptor } from "./loader.interceptor";
import { HttpErrorInterceptor } from "./error.interceptor";

export const INTERCEPTOR_PROVIDERS = [
  { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }
];
