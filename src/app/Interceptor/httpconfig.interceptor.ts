import { Injectable, Injector } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
  HttpClient,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";
import { PetfinderService } from "../service/petfinder.service";
import { LoggerService } from "../service/logger.service";

const BASE_URL = "https://api.petfinder.com/v2";
const CLIENT_ID = "Cstk7ayhA5di3IfCcSykTlC2JtEtHRieCXO6mSteTjAR1oHc5k";
const CLIENT_SECRET = "Q0s1TZMkRGDuc75tOWskj9v3pPLcloGns5jO8Xr2";

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  headers: HttpHeaders;

  constructor(private loggerService: LoggerService, private http: HttpClient) {}

  // getTokenfromApi() {
  //   this.http
  //     .post(`${BASE_URL}/oauth2/token`, {
  //       grant_type: "client_credentials",
  //       client_id: CLIENT_ID,
  //       client_secret: CLIENT_SECRET
  //     })
  //     .subscribe((res: any) => {
  //       localStorage.setItem("token", res.access_token);
  //     });
  // }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let token = localStorage.getItem("token");
    this.loggerService.info("Token is", token);

    const headerconfig = {};
    if (token) {
      headerconfig["Authorization"] = `Bearer ${token}`;
      request = request.clone({
        setHeaders: headerconfig
      });
      return next.handle(request);
      // .pipe(
      //  retry(1),
      //   catchError((error: HttpErrorResponse) => {
      //     if (error.status === 401) {
      //       if (error.error.message == "Token is expired") {
      //         //TODO: Token refreshing
      //       } else {
      //         //Logout from account or do some other stuff
      //       }
      //     }
      //     return throwError(error);
      //   })
      // );
    } else {
      this.http
        .post(`${BASE_URL}/oauth2/token`, {
          grant_type: "client_credentials",
          client_id: CLIENT_ID,
          client_secret: CLIENT_SECRET
        })
        .subscribe((res: any) => {
          token = res.access_token;
          localStorage.setItem("token", token);
          this.headers = new HttpHeaders()
            .set("Content-Type", "application/json")
            .set("authorization", "Bearer " + token);
        });
    }

    // if (token) {
    //   headerconfig["Authorization"] = `Bearer ${token}`;
    //   request = request.clone({
    //     setHeaders: headerconfig
    //   });
    // }

    // return next.handle(request).pipe(
    //   catchError((error: HttpErrorResponse) => {
    //     return throwError(error);
    //   })
    // );
  }
}
