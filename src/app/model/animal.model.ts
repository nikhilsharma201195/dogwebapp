export interface IAnimalBase {
  id: number;
  image?: string;
  name: string;
  age: string;
}

export interface IDog extends IAnimalBase {
  status: string;
  gender: string;
  type: string;
  description?: string;
  organization_id: number;
  email: string;
}

export interface IAnimal extends IAnimalBase {
  species: string;
  type: string;
  breeds: string;
}
