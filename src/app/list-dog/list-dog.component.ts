import { Component, OnInit } from "@angular/core";
import { PetfinderService } from "../service/petfinder.service";
import { IDog } from "../model/animal.model";
import { IAnimal } from "../model/animal.model";
import { Router } from "@angular/router";
import { element } from "@angular/core/src/render3";
import { AlertService } from "src/app/service/alert.service";
import { LoggerService } from "../service/logger.service";

@Component({
  selector: "app-list-dog",
  templateUrl: "./list-dog.component.html",
  styleUrls: ["./list-dog.component.scss"]
})
export class ListDogComponent implements OnInit {
  animals: IAnimal[];
  dogs: IDog[];
  error: string;

  constructor(
    private router: Router,
    public petFinderService: PetfinderService,
    public alertservice: AlertService,
    public logger: LoggerService
  ) {
    this.logger.info("ListDogComponent: logger.info");
  }

  ngOnInit() {
    this.getAnimalsList();
  }

  addDog(): void {
    this.router.navigate(["add-dog"]);
  }

  getAnimalsList() {
    // this.petFinderService.isLoading = true;
    this.petFinderService.getAllAnimals().subscribe(
      (data: any) => {
        // this.petFinderService.isLoading = false;
        this.animals = data.animals;
        this.alertservice.showSuccess(`Animals list loaded Succesfully`);
        this.logger.info(this.animals, "Data");
      },
      error => {
        this.error = error;
        // this.petFinderService.isLoading = false;
      }
    );
  }

  trackByFn(index, item) {
    return index; // or item.id
  }
}
