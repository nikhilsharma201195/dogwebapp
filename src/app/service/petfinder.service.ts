import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { IDog } from "../model/animal.model";
import { Observable, throwError } from "rxjs";
import { retry, catchError } from "rxjs/operators";
import { IAnimal } from "../model/animal.model";

const BASE_URL = "https://api.petfinder.com/v2";

@Injectable()
export class PetfinderService {

  // isLoading: boolean;
  headers: HttpHeaders;
  breeds: any;

  constructor(private http: HttpClient) {}

  // getTokenFromAPI() {
  //   this.http
  //     .post(`${BASE_URL}/oauth2/token`, {
  //       grant_type: "client_credentials",
  //       client_id: CLIENT_ID,
  //       client_secret: CLIENT_SECRET
  //     })
  //     .subscribe((res: any) => {
  //       localStorage.setItem("token", res.access_token);
  //     });
  // }

  // setAuthorizationHeader() {
  //   let token = localStorage.getItem("token");
  //   this.headers = new HttpHeaders()
  //     .set("Content-Type", "application/json")
  //     .set("authorization", "Bearer " + token);
  // }

  getBreeds() {
    return this.http.get<IDog[]>(`${BASE_URL}/types/dog/breeds`);
  }

  getDogsByBreed(breed: string) {
    return this.http.get<IDog[]>(`${BASE_URL}/animals?breed=${breed}`);
  }

  getAllAnimals() {
    // this.setAuthorizationHeader();
    return this.http
      .get<IAnimal[]>(`${BASE_URL}/animals`, {
        // headers: this.headers
      })
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  handleError(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
