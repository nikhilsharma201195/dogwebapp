import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegisterComponent } from "./register/register/register.component";
import { LoginComponent } from "./login/login/login.component";
import { AddDogComponent } from "./add-dog/add-dog/add-dog.component";
import { ListDogComponent } from "./list-dog/list-dog.component";
import { NotFoundComponent } from "./not-found/not-found.component";

const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  { path: "add-dog", component: AddDogComponent },
  { path: "404-notfound", component: NotFoundComponent },
  { path: "list-dog", component: ListDogComponent },
  { path: "**", redirectTo: "404-notfound" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
